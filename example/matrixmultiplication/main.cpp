#include <A2WS.hpp>
#include <mpi.h>

#include <chrono>
#include <cstring>
#include <iostream>
#include <ostream>

#include "mxm.h"
#include "utilis.h"

int main(int argc, char **argv) {
  setbuf(stdout, NULL);

  int mpi_rank, mpi_size, provided;
  MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &provided);
  MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);
  MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);

  int m_size, n_task;
  char input_path[100];
  double *A, *B, *C;

  switch (argc) {
    case 4:
      strcpy(input_path, argv[3]);
    case 3:
      n_task = std::stoi(argv[2]);
      m_size = std::stoi(argv[1]);
      break;
    default:
      printHelp();
  }

  try {
    A = new double[m_size * m_size];
    B = new double[m_size * m_size];
    C = new double[m_size * m_size];
  } catch (const std::exception &e) {
    std::cerr << e.what() << '\n';
  }

  initMatrixZero(C, m_size);

#ifdef CHECK_RUNTIME
  check_runtime_start();
#endif

  const char *sel_sched = argv[1];
  SDIS::Scheduler *sched = new A2WS(n_task);

  int itask;
  sched->getTask(&itask);
  while (!sched->end()){
    VERBOSE_PRINT("%s: Task %d", sel_sched, itask);
#if MATRIX_TYPE == 1
    initMatrixTest(A, m_size);
    initMatrixTest(B, m_size);
#elif MATRIX_TYPE == 2
    initMatrixRandom(A, m_size);
    initMatrixRandom(B, m_size);
#else  // MATRIX_TYPE == "READ"
    readMatrixBinaryFile(input_path, A, B, m_size, itask);
#endif
    matrixMultiplication(A, B, C, m_size, sched);
    sched->getTask(&itask);
  }

#ifdef CHECK_RUNTIME
  check_runtime_end();
#endif

#if MATRIX_TYPE == 1
  checkTestMatrix(C, m_size, n_task);
#endif

  delete[] A;
  delete[] B;
  delete[] C;

  delete sched;
  MPI_Finalize();

  return 0;
}
