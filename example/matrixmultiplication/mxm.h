#pragma once

#include <A2WS.hpp>

#include "mxm.h"
#include "utilis.h"

#ifndef NUM_REPETITIONS
#define NUM_REPETITIONS 8
#endif

#define CHECK_RUTIME

#ifndef MATRIX_TYPE
// #define MATRIX_TYPE 0  // Read Matrix from file
// #define MATRIX_TYPE 1  // Matrix of testes
#define MATRIX_TYPE 2  // Random Matrix
#endif

void printHelp() {
#if MATRIX_TYPE == 0
  std::cout << "Usage: mpiexec -np <number_of_processes> ./mxm_cham "
               "<matrix_size> <number_of_tasks> <input_path>"
            << std::endl;
#else
  std::cout << "Usage: mpiexec -np <number_of_processes> ./mxm_cham "
               "<matrix_size> <number_of_tasks>"
            << std::endl;
#endif
  exit(1);
}

void checkTestMatrix(double *matrix, int m_size, int n_task) {
  for (int i = 0; i < m_size; i++) {
    for (int j = 0; j < m_size; j++) {
      if (matrix[i * m_size + j] != (m_size * n_task)) {
        std::cerr << "Error: matrix[" << i << "][" << j << "] = " << matrix[i * m_size + j]
                  << " != " << m_size << std::endl;
        exit(1);
      }
    }
  }
}

inline void initMatrixZero(double *matrix, int m_size) {
  memset(matrix, 0, m_size * m_size * sizeof(double));
}

inline void initMatrixTest(double *matrix, int m_size) {
  memset(matrix, 1, m_size * m_size * sizeof(double));
}

void initMatrixRandom(double *matrix, int m_size) {
  for (int i = 0; i < m_size; i++) {
    for (int j = 0; j < m_size; j++) {
      matrix[i * m_size + j] = (double)rand() / RAND_MAX;
    }
  }
}

void readMatrixBinaryFile(const char *filename, double *matrix_a, double *matrix_b, int m_size,
                          int id) {
  const std::string s_filename = std::string(filename) + "/matrix_" + std::to_string(id) + ".bin";
  FILE *file                   = fopen(s_filename.c_str(), "rb");
  if (file == NULL) {
    std::cerr << "Error opening file " + s_filename << std::endl;
    exit(1);
  }

  fread(matrix_a, sizeof(double), m_size * m_size, file);
  fread(matrix_b, sizeof(double), m_size * m_size, file);

  fclose(file);
}

void matrixMultiplicationKernel(double *A, double *B, double *C, int size) {
  for (int i = 0; i < size; i++) {
    for (int j = 0; j < size; j++) {
      for (int k = 0; k < size; k++) {
        C[i * size + j] += A[i * size + k] * B[k * size + j];
      }
    }
  }
}

void matrixMultiplication(double *A, double *B, double *C, int size) {
  for (int iter = 0; iter < NUM_REPETITIONS; iter++) {
    matrixMultiplicationKernel(A, B, C, size);
  }
}

void matrixMultiplication(double *A, double *B, double *C, int size, SDIS::Scheduler *sched) {
  for (int iter = 0; iter < NUM_REPETITIONS; iter++) {
    matrixMultiplicationKernel(A, B, C, size);
    sched->refresh();
  }
}