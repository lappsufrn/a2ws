#pragma once

#include <mpi.h>

#include <iostream>

#ifdef VERBOSE
#define VERBOSE_PRINT(...)            \
  printf("Rank %i ", mpi_get_rank()); \
  printf(__VA_ARGS__);
#else
#define VERBOSE_PRINT(...)
#endif

int mpi_get_rank() {
  int rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  return rank;
}

#define check_runtime_start()  \
  MPI_Barrier(MPI_COMM_WORLD); \
  auto _start_ = std::chrono::high_resolution_clock::now()

#define check_runtime_end()                                                                   \
  auto _end_                              = std::chrono::high_resolution_clock::now();        \
  std::chrono::duration<double> _elapsed_ = _end_ - _start_;                                  \
  double _local_elapsed_                  = _elapsed_.count();                                \
  double _global_elapsed_;                                                                    \
  std::cout << sel_sched << ": R" << mpi_get_rank() << " Runtime " << _local_elapsed_         \
            << std::endl;                                                                     \
  MPI_Reduce(&_local_elapsed_, &_global_elapsed_, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD); \
  if (mpi_get_rank() == 0)                                                                    \
  std::cout << "Test " << sel_sched << " Runtime " << _global_elapsed_ << std::endl
