#pragma once

struct Coords {
  int x = 0;
  int y = 0;
};

struct Config {
  int nx       = 0;
  int ny       = 0;
  int dx       = 0;
  int dy       = 0;
  int c        = 0;
  int ns       = 0;
  double dt    = 0;
  double fpeak = 0;
};

class WaveSimulation {
  int nx, ny;  // Dimensions
  int dx, dy;  // Grid size
  int ns;      // Number of timesteps
  float dt;    // Time step
  float c;     // Propagation Velocity
  float frequency;
  float amplitude;

  float* cur_grid;
  float* old_grid;
  float* new_grid;

 public:
  WaveSimulation& operator=(const WaveSimulation& other) = delete;
  WaveSimulation(const WaveSimulation& other)            = delete;

  WaveSimulation();
  WaveSimulation(int nx, int ny, int dx, int dy, int ns, float dt, float c, float frequency,
                 float amplitude);
  explicit WaveSimulation(const Config& config);
  ~WaveSimulation();

  void zeroGrid();
  void simulate(const Coords& c_src);
  inline float source(float time, bool apply);

 private:
  int id(int i, int j) const { return i + j * nx; };
  void updateGrid(int t, const Coords& c_src);
  void switchGrid();
};
