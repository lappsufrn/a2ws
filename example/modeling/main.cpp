
/**
 * @example main-modeling.cpp
 * This is a example of ScheDIS applied to 2D modeling
 */
#include <mpi.h>

#include "Modeling.h"
#include "A2WS.hpp"
#include "WaveSimulation.h"

int main(int argc, char *argv[]) {
  Config config;
  config.fpeak = 10.0;
  config.dt    = 0.001;
  config.ns    = 2000;
  config.nx    = 100;
  config.ny    = 100;
  config.dx    = 10;
  config.dy    = 10;
  config.c     = 1500;

  int nsrc = 100;

  MPI_Init(&argc, &argv);

  const char *sel_sched = argv[1];
  SDIS::Scheduler *sched = new A2WS(nsrc);
  WaveSimulation *wsim = new WaveSimulation(config);
  Modeling *model      = new Modeling(nsrc, *sched, *wsim, config);

  model->run();

  delete model;
  delete wsim;

  delete sched;
  MPI_Finalize();

  return 0;
}
