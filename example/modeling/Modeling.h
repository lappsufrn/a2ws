#pragma once

#include <A2WS.hpp>

#include "WaveSimulation.h"

struct Config;
struct Coords;

class WaveSimulation;

class Modeling {
  int nsrc;
  Config fconfig;
  Coords *src_coords;
  WaveSimulation &fwsim;
  SDIS::Scheduler &fsched;

 public:
  Modeling() = delete;
  Modeling(int nsrc, SDIS::Scheduler &sched, WaveSimulation &wsim, const Config &config);
  ~Modeling();

  Modeling &operator=(const Modeling &other) = delete;
  Modeling(const Modeling &other)            = delete;

  void run();
};
