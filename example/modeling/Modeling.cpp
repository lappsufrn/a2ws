#include "Modeling.h"

#include <iostream>

#include "WaveSimulation.h"

Modeling::Modeling(int nsrc, SDIS::Scheduler &sched, WaveSimulation &wsim, const Config &config)
    : nsrc(nsrc), fsched(sched), fwsim(wsim), fconfig{config} {
  src_coords = new Coords[nsrc];

  for (int i = 0; i < nsrc; i++) {
    src_coords[i].x = i * (fconfig.nx / nsrc);
    src_coords[i].y = 0;
  }
}

Modeling::~Modeling() { delete[] src_coords; }

void Modeling::run() {
  int ishot;

  fsched.getTask(&ishot);
  while (!fsched.end()) {
    std::cout << "--- Running shot " << ishot << std::endl;
    fwsim.simulate(src_coords[ishot]);
    fsched.getTask(&ishot);
  }
}

/**
 * @example Modeling.cpp
 * This is a example of modeling applied to 2D modeling
 */
