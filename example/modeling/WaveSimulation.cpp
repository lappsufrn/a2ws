#include "WaveSimulation.h"

#include <stdio.h>

#include <cmath>

WaveSimulation::WaveSimulation(int nx, int ny, int dx, int dy, int ns, float dt, float c,
                               float frequency, float amplitude)
    : nx(nx),
      ny(ny),
      dx(dx),
      dy(dy),
      ns(ns),
      dt(dt),
      c(c),
      frequency(frequency),
      amplitude(amplitude) {
  cur_grid = new float[nx * ny];
  old_grid = new float[nx * ny];
  new_grid = new float[nx * ny];

  zeroGrid();
}

WaveSimulation::WaveSimulation()
    : WaveSimulation(100, 100, 10, 10, 2000, 0.001, 1500.0, 10.0, 10.0) {}

WaveSimulation::WaveSimulation(const Config& config)
    : WaveSimulation(config.nx, config.ny, config.dx, config.dy, config.ns, config.dt, config.c,
                     config.fpeak, config.fpeak) {}

WaveSimulation::~WaveSimulation() {
  delete[] cur_grid;
  delete[] old_grid;
  delete[] new_grid;
}

void WaveSimulation::zeroGrid() {
  int index;
  for (int i = 0; i < nx; i++) {
    for (int j = 0; j < ny; j++) {
      index           = id(i, j);
      cur_grid[index] = 0.0;
      old_grid[index] = 0.0;
      new_grid[index] = 0.0;
    }
  }
}

void WaveSimulation::simulate(const Coords& c_src) {
  zeroGrid();
  for (int i = 0; i < ns; i++) {
    updateGrid(i, c_src);
    switchGrid();
  }
}

void WaveSimulation::updateGrid(int t, const Coords& c_src) {
  float alphax = pow(c * dt, 2) / pow(dx, 2);
  float alphay = pow(c * dt, 2) / pow(dy, 2);
  float lapla_x, lapla_y;
  int index;

  for (int i = 2; i < nx - 2; ++i) {
    for (int j = 2; j < ny - 2; ++j) {
      lapla_x = cur_grid[id(i + 2, j)] - 4.0 * cur_grid[id(i + 1, j)] + 6.0 * cur_grid[id(i, j)] -
                4.0 * cur_grid[id(i - 1, j)] + cur_grid[id(i - 2, j)];
      lapla_y = cur_grid[id(i, j + 2)] - 4.0 * cur_grid[id(i, j + 1)] + 6.0 * cur_grid[id(i, j)] -
                4.0 * cur_grid[id(i, j - 1)] + cur_grid[id(i, j - 2)];

      index           = id(i, j);
      new_grid[index] = 2.0 * cur_grid[index] - old_grid[index] + alphax * lapla_x +
                        alphay * lapla_y + source(t, i == c_src.x && j == c_src.y);
    }
  }
}

void WaveSimulation::switchGrid() {
  float* aux = old_grid;
  old_grid   = cur_grid;
  cur_grid   = new_grid;
  new_grid   = aux;
}

inline float WaveSimulation::source(float time, bool apply) {
  return apply ? amplitude * (1 - 2 * pow(M_PI * frequency * time, 2)) *
                     exp(-pow(M_PI * frequency * time, 2))
               : 0.0;
}
