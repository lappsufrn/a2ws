
#include "Statistics.hpp"

#ifdef STATS
#include <mpi.h>

#include <iostream>

Statistics::Statistics() {}

Statistics::~Statistics() {}

void Statistics::start() { m_time = MPI_Wtime(); }

void Statistics::end() { m_totalTime = MPI_Wtime() - m_time; }

void Statistics::start_getTask() { m_time_gt = MPI_Wtime(); }

void Statistics::end_getTask() {
  m_getTaskTimeSum += MPI_Wtime() - m_time_gt;
  m_getTaskCount++;
}

void Statistics::start_workStealing() { m_time_ws = MPI_Wtime(); }

void Statistics::end_workStealing() {
  m_workStealingTimeSum += MPI_Wtime() - m_time_ws;
  m_workStealingCount++;
}

void Statistics::success_steal(int nts) {
  m_successStealCount++;
  m_stolenTasks += static_cast<unsigned>(nts);
}

void Statistics::fail_steal() { m_failStealCount++; }

void Statistics::end_token() { m_tokenDelaySum += MPI_Wtime() - m_time_ws; }

void Statistics::start_refresh() { m_time_rf = MPI_Wtime(); }

void Statistics::end_refresh(bool flag) {
  m_refreshTimeSum += MPI_Wtime() - m_time_rf;
  if (flag) m_successrefreshCount++;
  else m_failrefreshCount++;
}

void Statistics::print() {
  int rank = 0;
  int size = 0;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);

  const char* const sdis  = "[ScheDIS]\t";
  const char* const sdisr = "[ScheDIS][R\t";

  unsigned g_successStealCount   = 0;
  unsigned g_failStealCount      = 0;
  unsigned g_stolenTasks         = 0;
  double g_tokenDelaySum         = 0.0;
  double g_workStealingTimeSum   = 0.0;
  unsigned g_workStealingCount   = 0;
  double g_getTaskTimeSum        = 0.0;
  unsigned g_getTaskCount        = 0;
  double g_totalTimeMin          = 0.0;
  double g_totalTimeMax          = 0.0;
  double g_refreshTimeSum        = 0.0;
  unsigned g_successrefreshCount = 0;
  unsigned g_failrefreshCount    = 0;
  double g_totalTime             = 0.0;

  MPI_Reduce(&m_successStealCount, &g_successStealCount, 1, MPI_UNSIGNED, MPI_SUM, 0,
             MPI_COMM_WORLD);
  MPI_Reduce(&m_failStealCount, &g_failStealCount, 1, MPI_UNSIGNED, MPI_SUM, 0, MPI_COMM_WORLD);
  MPI_Reduce(&m_stolenTasks, &g_stolenTasks, 1, MPI_UNSIGNED, MPI_SUM, 0, MPI_COMM_WORLD);
  MPI_Reduce(&m_tokenDelaySum, &g_tokenDelaySum, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
  MPI_Reduce(&m_workStealingTimeSum, &g_workStealingTimeSum, 1, MPI_DOUBLE, MPI_SUM, 0,
             MPI_COMM_WORLD);
  MPI_Reduce(&m_workStealingCount, &g_workStealingCount, 1, MPI_UNSIGNED, MPI_SUM, 0,
             MPI_COMM_WORLD);
  MPI_Reduce(&m_getTaskTimeSum, &g_getTaskTimeSum, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
  MPI_Reduce(&m_getTaskCount, &g_getTaskCount, 1, MPI_UNSIGNED, MPI_SUM, 0, MPI_COMM_WORLD);
  MPI_Reduce(&m_totalTime, &g_totalTimeMax, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
  MPI_Reduce(&m_totalTime, &g_totalTimeMin, 1, MPI_DOUBLE, MPI_MIN, 0, MPI_COMM_WORLD);
  MPI_Reduce(&m_refreshTimeSum, &g_refreshTimeSum, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
  MPI_Reduce(&m_successrefreshCount, &g_successrefreshCount, 1, MPI_UNSIGNED, MPI_SUM, 0,
             MPI_COMM_WORLD);
  MPI_Reduce(&m_failrefreshCount, &g_failrefreshCount, 1, MPI_UNSIGNED, MPI_SUM, 0, MPI_COMM_WORLD);
  MPI_Reduce(&m_totalTime, &g_totalTime, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);

  // cout << sdis << rank << "\t] Get_Task_Time: " << g_getTaskTimeSum / g_getTaskCount << endl;

  if (rank == 0) {
    const double imbalance            = (g_totalTimeMax - g_totalTimeMin) / g_totalTime;
    const double refreshOverhead      = g_refreshTimeSum / g_totalTime;
    const double workStealingOverhead = g_workStealingTimeSum / g_totalTime;
    const double getTaskOverhead      = g_getTaskTimeSum / g_totalTime;
    const double tokenOverhead        = g_tokenDelaySum / g_totalTime;

    using std::cout;
    using std::endl;

    cout << "[ScheDIS] Steal_Statistics" << endl;
    if (g_successStealCount) cout << sdis << "Successful_Steals: " << g_successStealCount << endl;
    if (g_failStealCount) cout << sdis << "Failed_Steals: " << g_failStealCount << endl;
    if (g_stolenTasks) cout << sdis << "Stolen_Tasks: " << g_stolenTasks << endl;
    if (g_tokenDelaySum) cout << sdis << "Token_Delay_Sum: " << g_tokenDelaySum << endl;
    if (tokenOverhead) cout << sdis << "Token_Overhead: " << tokenOverhead << endl;
    if (g_refreshTimeSum) cout << sdis << "Refresh_Time_Sum: " << g_refreshTimeSum << endl;
    if (refreshOverhead) cout << sdis << "Refresh_Overhead: " << refreshOverhead << endl;
    if (g_successrefreshCount)
      cout << sdis << "Successful_Refresh: " << g_successrefreshCount << endl;
    if (g_failrefreshCount) cout << sdis << "Failed_Refresh: " << g_failrefreshCount << endl;
    if (g_workStealingTimeSum)
      cout << sdis << "Work_Stealing_Time_Sum: " << g_workStealingTimeSum << endl;
    if (g_workStealingCount) cout << sdis << "Work_Stealing_Count: " << g_workStealingCount << endl;
    if (workStealingOverhead)
      cout << sdis << "Work_Stealing_Overhead: " << workStealingOverhead << endl;
    cout << sdis << "Get_Task_Time_Sum: " << g_getTaskTimeSum << endl;
    cout << sdis << "Get_Task_Count: " << g_getTaskCount << endl;
    cout << sdis << "Get_Task_Overhead: " << getTaskOverhead << endl;
    cout << sdis << "Total_Time_Max: " << g_totalTimeMax << endl;
    cout << sdis << "Total_Time_Min: " << g_totalTimeMin << endl;
    cout << sdis << "Total_Time: " << g_totalTime << endl;
    cout << sdis << "Imbalance: " << imbalance << endl;
  }
}

void Statistics::reset() {
  m_successStealCount   = 0;
  m_failStealCount      = 0;
  m_stolenTasks         = 0;
  m_tokenDelaySum       = 0.0;
  m_workStealingTimeSum = 0.0;
  m_workStealingCount   = 0;
  m_getTaskTimeSum      = 0.0;
  m_getTaskCount        = 0;
  m_totalTime           = 0.0;
  m_refreshTimeSum      = 0.0;
  m_successrefreshCount = 0;
  m_failrefreshCount    = 0;
}

#else
void Statistics::start() {}
void Statistics::end() {}
void Statistics::print() {}
void Statistics::reset() {}
void Statistics::start_getTask() {}
void Statistics::end_getTask() {}
void Statistics::start_workStealing() {}
void Statistics::end_workStealing() {}
void Statistics::success_steal(int nts) {}
void Statistics::fail_steal() {}
void Statistics::end_token() {}
void Statistics::start_refresh() {}
void Statistics::end_refresh(bool flag) {}
#endif