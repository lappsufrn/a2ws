#include "../include/a2ws/TaskList.hpp"

#include <cstring>
#include <stdexcept>

#include "Tools.h"

namespace SDIS {

TaskList::TaskList(int ntask) : TaskList(ntask, sizeof(int)) {}

TaskList::TaskList(int ntask, int task_size)
    : m_taskSize(task_size), m_maxSize(ntask), m_mode(DEFAULT), m_listType(MPI_DATATYPE_NULL) {

  VERBOSE_PRINT("TaskList normal mode: Max size %i task size %i\n", m_maxSize, m_taskSize);

  if (m_maxSize <= 0) {
    throw std::invalid_argument(SDIS_NTASK_ERROR_MESSAGE);
  }
  if (m_taskSize <= 0) {
    throw std::invalid_argument(SDIS_TASK_SIZE_ERROR_MESSAGE);
  }

  try {
    p_limits   = new Limits[1];
    p_taskList = new std::byte[m_maxSize * (m_headerSize + m_taskSize) + 1];
  } catch (const std::exception& e) {
    std::cerr << e.what() << '\n';
  }

#ifdef DEBUG
  p_listTasks_end = p_taskList + m_maxSize * m_taskSize + 1;
  DEBUG_PRINT("TaskList Memory Allocated from %p to %p", static_cast<void*>(p_taskList),
              p_listTasks_end);
#endif

  this->clear();
}

TaskList::TaskList(int ntask, MPI_Datatype datatype) : TaskList(ntask, datatype, DEFAULT) {}

TaskList::TaskList(int ntask, MPI_Datatype datatype, ListMode mode)
    : m_taskSize(MyMPI::datatype_size(datatype)),
      m_maxSize(ntask),
      m_mode(mode),
      m_listType(datatype) {

  if (m_maxSize <= 0) {
    throw std::invalid_argument(SDIS_NTASK_ERROR_MESSAGE);
  }
  if (m_taskSize <= 0) {
    throw std::invalid_argument(SDIS_TASK_SIZE_ERROR_MESSAGE);
  }

  if (m_mode == DEFAULT) {
    VERBOSE_PRINT("TaskList normal mode: Max size %i task size %i\n", m_maxSize, m_taskSize);

    p_limits   = new Limits[1];
    p_taskList = new std::byte[m_maxSize * (m_headerSize + m_taskSize) + 1];

  } else if (m_mode == MPIWIN) {
    VERBOSE_PRINT("TaskList MPI mode: Max size %i task size %i\n", m_maxSize, m_taskSize);
  
    MPI_Win_allocate(TOAINT(sizeof(Limits)), sizeof(Limits), MPI_INFO_NULL, MPI_COMM_WORLD, &(p_limits),
                     &(m_limitsWin));
    MPI_Win_allocate(TOAINT(m_maxSize * (m_headerSize + m_taskSize) + 1), m_taskSize, MPI_INFO_NULL,
                     MPI_COMM_WORLD, &(p_taskList), &(m_listWin));
    MPI_Win_create(&(p_limits->head), sizeof(int), sizeof(int), MPI_INFO_NULL, MPI_COMM_WORLD,
                   &m_headWin);
    MPI_Win_create(&(p_limits->tail), sizeof(int), sizeof(int), MPI_INFO_NULL, MPI_COMM_WORLD,
                   &m_tailWin);
  } else {
    throw std::invalid_argument(SDIS_QUEUE_MODE_ERROR_MESSAGE);
  }

  this->clear();
}

TaskList::~TaskList() {
  if (m_mode == DEFAULT) {
    delete[] p_limits;
    delete[] p_taskList;
  } else if (m_mode == MPIWIN) {
    MPI_Win_free(&m_listWin);
    MPI_Win_free(&m_headWin);
    MPI_Win_free(&m_tailWin);
    MPI_Win_free(&m_limitsWin);
  }
}

std::byte TaskList::operator[](int index) const {
  if (index < 0 || index >= m_maxSize) {
    throw std::out_of_range(SDIS_QUEUE_INDEX_ERROR_MESSAGE);
  }
  return p_taskList[(m_headerSize + m_taskSize) * index + m_headerSize];
}

void TaskList::move_tail(int n) {
  if (p_limits->tail + n <= 0 || p_limits->tail + n > m_maxSize) {
    throw std::out_of_range(SDIS_QUEUE_INDEX_ERROR_MESSAGE);
  }
  p_limits->tail += n;
}

void TaskList::back_push(const void* data) {
  if (p_limits->tail == m_maxSize) {
    throw std::out_of_range(SDIS_QUEUE_FULL_MESSAGE);
  }

  std::memcpy(&p_taskList[(m_headerSize + m_taskSize) * p_limits->tail], data,
              m_headerSize + m_taskSize);
  p_limits->tail++;
}

void TaskList::front_pop(void*& data) {
  if (empty()) {
    throw std::out_of_range(SDIS_QUEUE_EMPTY_MESSAGE);
  }

  if (data == nullptr) {
    DEBUG_PRINT("Copying pointer...");
    data = &p_taskList[(m_headerSize + m_taskSize) * p_limits->head];
  } else {
    DEBUG_PRINT("Copying data...");
    std::memcpy(data, &p_taskList[(m_headerSize + m_taskSize) * p_limits->head],
                (m_headerSize + m_taskSize));
  }

  p_limits->head++;
}

void TaskList::back_copy(const void* data, int size) {
  if (p_limits->tail + size > m_maxSize) {
    throw std::out_of_range(SDIS_QUEUE_FULL_MESSAGE);
  }

  std::memcpy(&p_taskList[(m_headerSize + m_taskSize) * p_limits->tail], data,
              size * (m_headerSize + m_taskSize));
  p_limits->tail += size;
};

void TaskList::clear() {
  memset(p_taskList, -1, m_maxSize * m_taskSize);
  p_limits->head = 0;
  p_limits->tail = 0;
}

int TaskList::size() const { return p_limits->tail - p_limits->head; }

bool TaskList::empty() const { return p_limits->head == p_limits->tail; }

void TaskList::adjust() {
  if (p_limits->tail < p_limits->head) {
    p_limits->tail = p_limits->head;
  }
}

}  // namespace SDIS