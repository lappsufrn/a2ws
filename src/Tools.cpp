#include "Tools.h"

#include <cstdio>
#include <iostream>

int MyMPI::rank() {
  int rank = -1;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  return rank;
}

int MyMPI::size() {
  int _size = -1;
  MPI_Comm_size(MPI_COMM_WORLD, &_size);
  return _size;
}

int MyMPI::datatype_size(MPI_Datatype datatype) {
  int _size = -1;
  MPI_Type_size(datatype, &_size);
  return _size;
}

void MyMPI::debug_errhandler(MPI_Comm *comm, int *errcode, ...) {  // NOLINT
  char err_string[MPI_MAX_ERROR_STRING];
  int len = 0;
  MPI_Error_string(*errcode, err_string, &len);
  fprintf(stderr, "Custom Error Handler: %s\n", err_string);

  // This will cause a segmentation fault to be detected by the gdb
  int *ptr = nullptr;              // cppcheck-suppress nullPointer // NOLINT
  std::cout << *ptr << std::endl;  // cppcheck-suppress nullPointer // NOLINT
  MPI_Abort(*comm, *errcode);
}
