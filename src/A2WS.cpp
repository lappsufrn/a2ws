#include "../include/A2WS.hpp"

#include <chrono>
#include <cmath>
#include <iostream>
#include <random>
#include <stdexcept>
#include <string>
#include <vector>

#include "Statistics.hpp"
#include "Tools.h"
#include "comm/Ring.hpp"

#ifndef offsetof
#define offsetof(type, member) ((size_t) & (((type *)0)->member))
#endif

//! ### Constructores and Destructors ###

A2WS::A2WS(int n_task) : A2WS(n_task, MyMPI::size(), MyMPI::rank()) {}

A2WS::A2WS(int n_task, int n_process, int id) : A2WS(n_task, n_process, id, 0) {}

A2WS::A2WS(int n_task, int n_process, int id, int radius_size)
    : m_nTasks_g(n_task), m_nProcess(n_process), m_ID(id) {
  if (n_task <= 0) {
    throw std::invalid_argument(SDIS_NTASK_ERROR_MESSAGE);
  }
  if (n_process <= 0) {
    throw std::invalid_argument(SDIS_NPROCESS_ERROR_MESSAGE);
  }
  if (id < 0 || id >= n_process) {
    throw std::invalid_argument(SDIS_ID_ERROR_MESSAGE);
  }

  Statistics::start();

  int i;
  int lnt, mft, nt_np = n_task % m_nProcess;

  /** Atributes Initialization **/
  this->m_taskCount = -1;
  this->m_taskTimeSum = 0;
  this->m_prevID = (m_nProcess + id - 1) % m_nProcess;
  this->m_nextID = (id + 1) % m_nProcess;
  this->m_nextID = (m_ID + 1) % m_nProcess;
  this->m_rateSum = 0;
  this->m_taskSum = 0;

  if (m_ID < nt_np) {
    lnt = n_task / m_nProcess + 1;
    mft = m_ID * lnt;
  } else {
    lnt = n_task / m_nProcess;
    mft = nt_np * (lnt + 1) + (m_ID - nt_np) * lnt;
  }

  p_excepTimeInfo = new double[m_nProcess];

  if (radius_size == 0) {
    // p_infocomm = new Butterfly(m_nProcess, m_ID);
    p_infocomm = new Ring(m_nProcess, m_ID, 0, InfoComm::GLOBAL_COMM);
  } else {
    p_infocomm = new Ring(m_nProcess, m_ID, radius_size, InfoComm::LOCAL_COMM);
  }

  // Windows
  MPI_Win_allocate(m_nProcess * sizeof(WSNodeInfo), sizeof(WSNodeInfo), MPI_INFO_NULL,
                   MPI_COMM_WORLD, &p_procsInfo, &m_procsInfoWin);

  p_queue = new SDIS::TaskList(m_nTasks_g, MPI_INT, SDIS::TaskList::MPIWIN);

  // FIXME: This is a workaround to avoid a bug in MPI_Win_lock
  for (i = 0; i < m_nProcess; i++) {
    MPI_Win_lock(MPI_LOCK_EXCLUSIVE, i, 0, p_queue->get_limits_win());
    MPI_Win_unlock(i, p_queue->get_limits_win());
  }

  // list of number of info per process
  MPI_Win_lock(MPI_LOCK_EXCLUSIVE, m_ID, 0, m_procsInfoWin);
  for (i = 0; i < m_nProcess; i++) {
    p_procsInfo[i].num_tasks = (int)(n_task / m_nProcess + (i < nt_np));
    p_procsInfo[i].task_time = 0.0;
    p_procsInfo[i].flag = FlagMode::NO_UPDATE;
  }
  MPI_Win_unlock(m_ID, m_procsInfoWin);

  // Add tasks for list
  MPI_Win_lock(MPI_LOCK_EXCLUSIVE, m_ID, 0, p_queue->get_limits_win());
  MPI_Win_lock(MPI_LOCK_EXCLUSIVE, m_ID, 0, p_queue->get_listWin());
  p_queue->clear();
  for (i = 0; i < lnt; i++) {
    const int aux = mft + i;
    p_queue->back_push(&aux);
  }
  MPI_Win_unlock(m_ID, p_queue->get_listWin());
  MPI_Win_unlock(m_ID, p_queue->get_limits_win());

  MPI_Barrier(MPI_COMM_WORLD);
  time_zero = MPI_Wtime();
}

A2WS::~A2WS() {
  Statistics::end();
  Statistics::print();
  Statistics::reset();
  delete[] p_excepTimeInfo;
  delete p_infocomm;
  delete p_queue;
  MPI_Win_free(&m_procsInfoWin);
}

//! ### Public Functions ###

void A2WS::getTask(void *task) {
#ifdef _OPENMP
#pragma omp single
#endif
  {
    Statistics::start_getTask();

    m_taskCount++;
    m_taskTimeSum = MPI_Wtime() - time_zero;
    refresh();

    // --- WORK STEALING
    if (m_taskCount > 0) {
      workstealing(task);
    }

    // --- GET TASK FROM ITS OWN LIST
    MPI_Win_lock(MPI_LOCK_EXCLUSIVE, m_ID, 0, p_queue->get_limits_win());
    if (!p_queue->empty()) {
      MPI_Win_lock(MPI_LOCK_SHARED, m_ID, 0, p_queue->get_listWin());
      p_queue->front_pop(task);
      MPI_Win_unlock(m_ID, p_queue->get_listWin());
    } else {
      m_end = true;
    }
    MPI_Win_unlock(m_ID, p_queue->get_limits_win());

    refresh();

    Statistics::end_getTask();
  }
}

bool A2WS::workstealing(void *task) {
  int vid = -1;    // victim m_ID
  int stolen = 0;  // flag

  Statistics::start_workStealing();

  do {
    double nts = calculateTasksToSteal();
    vid = findVictim(nts);
    if (vid >= 0) {
      stolen = stealTasks(vid, nts);
    }
  } while (exist(vid));

  Statistics::end_workStealing();

  return stolen;
}

bool A2WS::refresh() {
  bool flag = false;
#ifdef _OPENMP
#pragma omp single nowait
#endif
  {
    Statistics::start_refresh();

    refreshProcessInfo();
    flag = p_infocomm->updateInfo(p_procsInfo, m_procsInfoWin);

    Statistics::end_refresh(flag);
  }
  return flag;
}

//! ### Private Functions ###

void A2WS::refreshProcessInfo() {
  const double i_time = m_taskCount > 0 ? (m_taskTimeSum / (double)m_taskCount) : 0.0;

  MPI_Win_lock(MPI_LOCK_EXCLUSIVE, m_ID, 0, p_queue->get_limits_win());
  p_queue->adjust();
  const int i_task = p_queue->get_tail();
  MPI_Win_unlock(m_ID, p_queue->get_limits_win());

  MPI_Win_lock(MPI_LOCK_EXCLUSIVE, m_ID, 0, m_procsInfoWin);
  if (p_procsInfo[m_ID].task_time != i_time || p_procsInfo[m_ID].num_tasks != i_task) {
    p_procsInfo[m_ID].task_time = i_time;
    p_procsInfo[m_ID].num_tasks = i_task;
    p_procsInfo[m_ID].flag = FlagMode::UPDATE;
  }
  MPI_Win_unlock(m_ID, m_procsInfoWin);
}

int A2WS::randomVictim(const std::vector<int> &ranks, const std::vector<double> &weights) const {
  if (!weights.empty()) {
    unsigned seed1 = std::chrono::system_clock::now().time_since_epoch().count();
    std::default_random_engine generator(seed1 + m_ID);
    std::discrete_distribution<> distribution(weights.begin(), weights.end());
    return ranks[distribution(generator)];
  }
  return -1;
}

int A2WS::findVictim(double &nts) {
  if (nts <= 0.0) return -1;

  int victim = -1;
  int victim_r = -1;
  int victim_h = -1;
  double ntvd;
  double rate = 0;  //! Transfer rate ( > 0 Steal | < 0 Be Steal)
  double h_nts = 0;
  std::vector<double> weights;
  std::vector<int> ranks;
  std::vector<double> h_weights;
  std::vector<int> h_ranks;

  MPI_Win_lock(MPI_LOCK_SHARED, m_ID, 0, m_procsInfoWin);

  for (int i = p_infocomm->first(); i != p_infocomm->last(); i = p_infocomm->next(i)) {
    if (i != m_ID) {

      ntvd = n_task_deduction(i);
      rate = steal_task_rate(i);
      h_nts = calculateLocalTasksToSteal(i);

      if (rate < 0.0 && roundI(nts, i) > 0.0) {
        ranks.push_back(i);
        weights.push_back(1 / (rate + nts));
      }
      if (h_nts > 0.0 && roundI(h_nts, i) > 0.0) {
        h_ranks.push_back(i);
        h_weights.push_back(h_nts);
      }
    }
  }

  victim_r = randomVictim(ranks, weights);
  victim_h = randomVictim(h_ranks, h_weights);

  // Select first random victim
  if (exist(victim_r)) {
    // //! If victim do not have enough tasks
    ntvd = fabs(steal_task_rate(victim_r));
    if (nts > ntvd) nts = ntvd;

    nts = (double)roundI(nts, victim_r);
    if (nts > 0.0) victim = victim_r;
  }
  if (!exist(victim) && exist(victim_h)) {
    h_nts = calculateLocalTasksToSteal(victim_h);

    nts = (double)roundI(h_nts, victim_h);
    if (nts > 0.0) victim = victim_h;
  }
  MPI_Win_unlock(m_ID, m_procsInfoWin);

  return victim;
}

int A2WS::roundI(double nts, int victim) {
  const double nts_floor = floor(nts);
  const double nts_ceil = ceil(nts);
  const double t_m_1 = (p_procsInfo[m_ID].num_tasks + nts_floor) * p_procsInfo[m_ID].task_time;
  const double t_v_1 = (p_procsInfo[victim].num_tasks - nts_floor) * exception_time(victim);
  const double time_1 = t_m_1 > t_v_1 ? t_m_1 : t_v_1;
  const double t_m_2 = (p_procsInfo[m_ID].num_tasks + nts_ceil) * p_procsInfo[m_ID].task_time;
  const double t_v_2 = (p_procsInfo[victim].num_tasks - nts_ceil) * exception_time(victim);
  const double time_2 = t_m_2 > t_v_2 ? t_m_2 : t_v_2;
  if (time_1 < time_2) return nts_floor;
  return nts_ceil;
}

double A2WS::calculateTasksToSteal() {
  double nts = 0.0;
  this->m_rateSum = 0;
  this->m_taskSum = 0;

  // Decided how many task will steal. The task amount of probable victim will be deduced from
  // time_last_update info
  MPI_Win_lock(MPI_LOCK_SHARED, m_ID, 0, m_procsInfoWin);
  for (int i = p_infocomm->first(); i != p_infocomm->last(); i = p_infocomm->next(i)) {
    if (p_procsInfo[i].task_time > 0.0) {
      m_rateSum += 1 / p_procsInfo[i].task_time;
    } else {
      p_excepTimeInfo[i] = MPI_Wtime() - time_zero;
      m_rateSum += 1 / p_excepTimeInfo[i];
    }
    m_taskSum += p_procsInfo[i].num_tasks;
  }
  nts = steal_task_rate(m_ID);
  MPI_Win_unlock(m_ID, m_procsInfoWin);

  return nts;
}

inline double A2WS::calculateLocalTasksToSteal(int victim) {
  const double s_task = p_procsInfo[m_ID].num_tasks + p_procsInfo[victim].num_tasks;
  const double s_rate = 1 / p_procsInfo[m_ID].task_time + 1 / exception_time(victim);

  const double my_rate = 1 / p_procsInfo[m_ID].task_time;
  return (((my_rate * s_task) / s_rate) - p_procsInfo[m_ID].num_tasks);
}

int A2WS::stealTasks(int victim, int nts) {
  if (nts <= 0) return 0;

  int ntv = 0;   // Number of victim tasks
  int ntvs = 0;  // Number of victim stolen tasks
#ifdef VERBOSE
  int aux_nts = nts;
#endif

  SDIS::Limits vin, vout;
  vin.head = 0;
  vin.tail = -nts;

  DEBUG_PRINT("Trying steal Victim %i NTasks %i", victim, nts);

  // Get and Refresh victim info (Stolen tasks)
  MPI_Win_lock(MPI_LOCK_SHARED, victim, 0, p_queue->get_listWin());
  MPI_Win_lock(MPI_LOCK_EXCLUSIVE, victim, 0, p_queue->get_limits_win());
  MPI_Get_accumulate(&vin, 2, MPI_INT, &vout, 2, MPI_INT, victim, 0, 2, MPI_INT, MPI_SUM,
                     p_queue->get_limits_win());
  MPI_Win_flush(victim, p_queue->get_limits_win());

  // Check if info[victim] is updated. If not and the difference of tasks is
  // greater than the amount of overflow tasks stolen (nts - ntv), fix it.
  ntvs = p_procsInfo[victim].num_tasks - vout.tail;
  int fix = nts > ntvs ? ntvs : nts;
  if (fix > 0) {
    VERBOSE_ALERT("Exception: Fixed %i tasks. info[v] = %i and v.tail = %i \n", fix,
                  p_procsInfo[victim].num_tasks, vout.tail);
    nts -= fix;
    vin.tail = fix;
    MPI_Accumulate(&vin, 2, MPI_INT, victim, 0, 2, MPI_INT, MPI_SUM, p_queue->get_limits_win());
  }
  MPI_Win_unlock(victim, p_queue->get_limits_win());

  // If stolen tasks are greater than the victim really have, fix it.
  ntv = vout.tail - vout.head;
  if (nts > ntv) nts = ntv;

  if (nts > 0) {
    // Get victim tasks
    auto *vtemp = new std::byte[nts * p_queue->get_type_size()];
    MPI_Get(vtemp, nts, p_queue->get_list_type(), victim, (vout.tail - nts), nts,
            p_queue->get_list_type(), p_queue->get_listWin());
    MPI_Win_unlock(victim, p_queue->get_listWin());

    MPI_Win_lock(MPI_LOCK_EXCLUSIVE, m_ID, 0, p_queue->get_listWin());
    MPI_Win_lock(MPI_LOCK_EXCLUSIVE, m_ID, 0, p_queue->get_limits_win());
    p_queue->back_copy(vtemp, nts);
    MPI_Win_unlock(m_ID, p_queue->get_limits_win());
    MPI_Win_unlock(m_ID, p_queue->get_listWin());

    VERBOSE_PRINT(
        "STOLEN: m_ID = %d vid = %d nts = %d from %d ntv = %d ntvs = %d "
        "my_steal = %lf vic_steal = "
        "%lf local_steal = %d ltp = ( %s)\n",
        m_ID, victim, nts, aux_nts, ntv, ntvs, steal_task_rate(m_ID), steal_task_rate(victim),
        (int)calculateLocalTasksToSteal(victim),
        make_string(reinterpret_cast<int *>(vtemp), nts).c_str());

    MPI_Win_lock(MPI_LOCK_EXCLUSIVE, m_ID, 0, m_procsInfoWin);
    p_procsInfo[m_ID].num_tasks += nts;
    p_procsInfo[m_ID].flag = FlagMode::UPDATE;
    p_procsInfo[victim].num_tasks = vout.tail - nts;
    if (p_procsInfo[victim].task_time == 0.0 && vout.head > 0)
      p_procsInfo[victim].task_time = (MPI_Wtime() - time_zero) / vout.head;
    p_procsInfo[victim].flag = FlagMode::UPDATE;
    MPI_Win_unlock(m_ID, m_procsInfoWin);

    Statistics::success_steal(nts);

    delete[] vtemp;

    return 1;
  }
  MPI_Win_unlock(victim, p_queue->get_listWin());

  VERBOSE_PRINT("NOT STOLEN: m_ID = %d vid = %d nts = %d from %d ntv = %d ntvs = %d\n", m_ID,
                victim, nts, aux_nts, ntv, ntvs);

  if (p_procsInfo[victim].num_tasks != vout.tail) {
    MPI_Win_lock(MPI_LOCK_EXCLUSIVE, m_ID, 0, m_procsInfoWin);
    p_procsInfo[victim].num_tasks = vout.tail;
    if (p_procsInfo[victim].task_time == 0.0 && vout.head > 0)
      p_procsInfo[victim].task_time = (MPI_Wtime() - time_zero) / vout.head;
    p_procsInfo[victim].flag = FlagMode::UPDATE;
    MPI_Win_unlock(m_ID, m_procsInfoWin);
  }

  Statistics::fail_steal();

  return 0;
}

#if VERBOSE
void A2WS::print() const {
  std::cout << "ID " << m_ID << " m_nTasks_g=" << m_nTasks_g << "\t";
  MPI_Win_lock(MPI_LOCK_SHARED, m_ID, 0, m_procsInfoWin);
  switch (m_nProcess) {
    case 1:
      std::cout << "num_tasks : " << p_procsInfo[0].num_tasks << "\t";
      break;
    case 2:
      std::cout << "num_tasks: " << p_procsInfo[0].num_tasks << " " << p_procsInfo[1].num_tasks
                << "\t";
      break;
    case 3:
      std::cout << "num_tasks : " << p_procsInfo[0].num_tasks << " " << p_procsInfo[1].num_tasks
                << " " << p_procsInfo[2].num_tasks << "\t";
      break;
    default:
      std::cout << "num_tasks : " << p_procsInfo[0].num_tasks << " " << p_procsInfo[1].num_tasks
                << "..." << p_procsInfo[m_nProcess - 2].num_tasks << " "
                << p_procsInfo[m_nProcess - 1].num_tasks << "\t";
  }
  MPI_Win_unlock(m_ID, m_procsInfoWin);
  std::cout << "Task list: " << p_queue << "\t";
  std::cout << "" << std::endl;
}
#endif

std::string A2WS::NodesInfoToString() {
  std::string str = "[" + std::to_string(m_ID) + "] " + std::to_string(p_infocomm->first()) +
                    " -> " + std::to_string(p_infocomm->last()) + "\n";
  for (int i = p_infocomm->first(); i != p_infocomm->last(); i = p_infocomm->next(i)) {
    if (p_procsInfo[i].task_time == 0.0)
      str += std::to_string(i) + " = (" + std::to_string(p_procsInfo[i].num_tasks) + ", " +
             std::to_string(p_procsInfo[i].task_time) + ", " + std::to_string(p_procsInfo[i].flag) +
             ")\n";
  }
  return str;
}