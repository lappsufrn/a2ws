/**
 * @file Ring.hpp
 * @author João Batista Fernandes (jotabe.150@gmail.com)
 * @brief Implements the communication of information between processes needed for the A2 work
 * stealing algorithm.
 * @version 0.1
 * @date 2024-06-05
 *
 * @copyright Copyright (c) 2024
 *
 */
#pragma once

#include <mpi.h>

#include "../include/a2ws/InfoComm.hpp"

/**
 * @brief Class to manage the communication of information between processes.
 */
class Ring : public InfoComm {

  int m_size;             /** Number of processes */
  int m_first;            /** First Process ID */
  int m_last;             /** Last Process ID */
  const int m_ID;         /** Process ID */
  const int m_nextID;     /** Next Process ID */
  const int m_prevID;     /** Previous Process ID */
  const int m_nProcess;   /** Number of process */
  const CommMode m_mode;  /** Communication mode */
  WSNodeInfo *m_sendInfo; /** Information to be sent */

  typedef int (*F_Next)(int, int);
  F_Next f_next; /** Function to get the next process ID */

  MPI_Datatype m_infoLeftType;  /** MPI Datatype of left processes info */
  MPI_Datatype m_infoRigthType; /** MPI Datatype of right processes info */

  /**
   * @brief Check if the information needs to be updated.
   * @param info Information to be checked
   * @return true if the information needs to be updated, false otherwise
   */
  bool isUpdateNeeded(const WSNodeInfo *info) const;

 public:
  Ring(const Ring &)            = delete;
  Ring(Ring &&)                 = delete;
  Ring &operator=(const Ring &) = delete;
  Ring &operator=(Ring &&)      = delete;

  /**
   * @brief Construct a new Info Comm object
   * @param n_process
   * @param id
   * @param radius
   */
  Ring(int n_process, int id, int radius, CommMode mode);

  /**
   * @brief Destroy the Info Comm object
   */
  ~Ring() override;

  /**
   * @brief Get the first process ID
   * @return int
   */
  int first() const override { return m_first; }

  /**
   * @brief Get the last process ID
   * @return int
   */
  int last() const override { return m_last; }

  /**
   * @brief Get the next process ID in the ring.
   * @param id Process ID
   * @return int Next process ID
   */
  int next(int id) const override { return f_next(id, m_nProcess); }

  /**
   * @brief Get the previous process ID in the ring.
   * @param id Process ID
   * @return int Previous process ID
   */
  int size() const override { return m_size; }

  /**
   * @brief Get the communication mode
   * @return CommMode
   */
  CommMode mode() const override { return m_mode; }

  /**
   * @brief Send information to the neighborhood processes.
   * @param info Information list to be sent
   * @param win info mpi window
   * @return true if the information was sent, false otherwise
   */
  bool updateInfo(WSNodeInfo *info, const MPI_Win &win) override;
};
