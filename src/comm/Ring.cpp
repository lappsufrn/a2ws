#include "Ring.hpp"

#include <cmath>
#include <cstring>
#include <stdexcept>

#include "../Tools.h"

std::string NodesInfoToString(const WSNodeInfo *info, int n) {  
  std::string str = "";
  for (int i = 0; i < n; i++) {
    str += std::to_string(i) + " = (" + std::to_string(info[i].num_tasks) + ", " +
           std::to_string(info[i].task_time) + ", " + std::to_string(info[i].flag) +
           ")\n";
  }
  return str;
}

Ring::Ring(int n_process, int id, int radius, CommMode mode)  // NOLINT
    : m_ID(id),
      m_nextID((id + 1) % n_process),
      m_prevID((id - 1 + n_process) % n_process),
      m_nProcess(n_process),
      m_mode(mode) {

  if (radius < 0 || (2 * radius + 1) > m_nProcess) {
    throw std::invalid_argument(SDIS_RADIUS_ERROR_MESSAGE);
  }

  int i      = 0;
  int j      = 0;
  int l_size = 0;
  int r_size = 0;

  MPI_Alloc_mem(sizeof(WSNodeInfo) * m_nProcess, MPI_INFO_NULL, &m_sendInfo);  // NOLINT

  std::memset(m_sendInfo, 0, sizeof(WSNodeInfo) * m_nProcess);

  if (m_mode == GLOBAL_COMM) {
    l_size = (int)floor(((double)m_nProcess - 1.) / 2.) + 1;
    r_size = (int)ceil(((double)m_nProcess - 1.) / 2.) + 1;

    m_first = 0;
    m_last  = m_nProcess;
    m_size  = l_size + r_size - 1;

    f_next = [](int id, int n_process) -> int { return (id + 1); };
  } else if (m_mode == LOCAL_COMM) {
    l_size = radius + 1;
    r_size = radius + 1;

    m_first = (m_ID - radius + m_nProcess) % m_nProcess;
    m_last  = (m_ID + radius + 1) % m_nProcess;
    m_size  = l_size + r_size - 1;

    f_next = [](int id, int n_process) -> int { return (id + 1) % n_process; };
  } else {
    throw std::invalid_argument("Invalid communication mode");
  }

  const int sz = WSNodeInfo::size();
  int *lengths = new int[sz * m_nProcess];
  auto *disp   = new MPI_Aint[sz * m_nProcess];
  auto *types  = new MPI_Datatype[sz * m_nProcess];

  // RIGHT
  for (i = 0, j = m_ID; i < (r_size - 1); i++, j = (j + 1) % m_nProcess) {
    lengths[sz * i] = lengths[sz * i + 1] = lengths[sz * i + 2] = 1;

    for (int k = 0; k < sz; k++) lengths[sz * i + k] = 1;
    for (int k = 0; k < sz; k++) types[sz * i + k] = WSNodeInfo::type(k);
    for (int k = 0; k < sz; k++) disp[sz * i + k] = j * sizeof(WSNodeInfo) + WSNodeInfo::disp(k);
  }

  MPI_Type_create_struct(sz * (r_size - 1), lengths, disp, types, &m_infoRigthType);
  MPI_Type_commit(&m_infoRigthType);

  // LEFT
  for (i = 0, j = m_ID; i < (l_size - 1); i++, j = (j - 1 + m_nProcess) % m_nProcess) {
    for (int k = 0; k < sz; k++) disp[sz * i + k] = j * sizeof(WSNodeInfo) + WSNodeInfo::disp(k);
  }

  MPI_Type_create_struct(sz * (l_size - 1), lengths, disp, types, &m_infoLeftType);
  MPI_Type_commit(&m_infoLeftType);

  delete[] lengths;
  delete[] disp;
  delete[] types;
}

Ring::~Ring() {
  MPI_Free_mem(m_sendInfo);
  MPI_Type_free(&m_infoLeftType);
  MPI_Type_free(&m_infoRigthType);
}

bool Ring::isUpdateNeeded(const WSNodeInfo *info) const {
  for (int i = m_first; i != m_last; i = next(i)) {
    if (info[i].flag >= UPDATE) return true;
  }
  return false;
}

bool Ring::updateInfo(WSNodeInfo *info, const MPI_Win &win) {
  if (isUpdateNeeded(info)) {
    MPI_Win_lock(MPI_LOCK_SHARED, m_ID, 0, win);
    memcpy(m_sendInfo, info, sizeof(WSNodeInfo) * m_nProcess);
    MPI_Win_unlock(m_ID, win);

    MPI_Win_lock(MPI_LOCK_SHARED, m_prevID, 0, win);
    MPI_Put(m_sendInfo, 1, m_infoRigthType, m_prevID, 0, 1, m_infoRigthType, win);
    MPI_Win_unlock(m_prevID, win);

    MPI_Win_lock(MPI_LOCK_SHARED, m_nextID, 0, win);
    MPI_Put(m_sendInfo, 1, m_infoLeftType, m_nextID, 0, 1, m_infoLeftType, win);
    MPI_Win_unlock(m_nextID, win);

    MPI_Win_lock(MPI_LOCK_EXCLUSIVE, m_ID, 0, win);
    for (int i = m_first; i != m_last; i = next(i)) {
      if (m_sendInfo[i].flag == UPDATE) info[i].flag = NO_UPDATE;
    }
    MPI_Win_unlock(m_ID, win);

    return true;
  }
  return false;
}
