#pragma once

#include <mpi.h>
#include <string>

#ifdef VERBOSE
#define VERBOSE_PRINT(...)                  \
  printf("[ScheDIS:R%i]: ", MyMPI::rank()); \
  printf(__VA_ARGS__);
#define VERBOSE_ALERT(...)    \
  printf("\033[31m");         \
  VERBOSE_PRINT(__VA_ARGS__); \
  printf("\033[0m");
#else
#define VERBOSE_PRINT(...)
#define VERBOSE_ALERT(...)
#endif  // VERBOSE

#ifdef DEBUG
#define DEBUG_PRINT(...)                                                       \
  fprintf(stderr, "[ScheDIS:%s:%i:R%i]: ", __FILE__, __LINE__, MyMPI::rank()); \
  fprintf(stderr, __VA_ARGS__);                                                \
  fprintf(stderr, "\n")
#define MEM_CHECK(ptr, ptr_end)              \
  if (ptr > ptr_end) {                       \
    DEBUG_PRINT("TaskList Memory Overflow"); \
  }
#else
#define DEBUG_PRINT(...)
#define MEM_CHECK(ptr, ptr_end)
#endif  // VERBOSE

#define SDIS_NTASK_ERROR_MESSAGE "Number of tasks (n_task) should be greater than zero!"
#define SDIS_NPROCESS_ERROR_MESSAGE "Number of processes (n_process) should be greater than zero!"
#define SDIS_ID_ERROR_MESSAGE "Invalid process ID, must to be [0,n_process[!"
#define SDIS_RADIUS_ERROR_MESSAGE "Invalid radius, must to be [0,(n_process-1)/2]!"
#define SDIS_SCHED_TYPE_ERROR_MESSAGE "Scheduler type does not exist"
#define SDIS_REFRESH_MODE_ERROR_MESSAGE "Refresh mode type does not exist"
#define SDIS_QUEUE_MODE_ERROR_MESSAGE "Task list mode type does not exist"
#define SDIS_QUEUE_FULL_MESSAGE "Task list is full!"
#define SDIS_QUEUE_EMPTY_MESSAGE "Task list is empty!"
#define SDIS_QUEUE_DEFAULT_MODE_ERROR_MESSAGE "Method not available in the DEFAULT mode!"
#define SDIS_TASK_SIZE_ERROR_MESSAGE "Task size should be greater than zero!"
#define SDIS_QUEUE_INDEX_ERROR_MESSAGE "Index out of bounds!"

#define TOAINT(x) static_cast<MPI_Aint>(x)
#define TOINT(x) static_cast<int>(x)
#define TOBOOL(x) static_cast<bool>(x)

/**
 * @brief Namespace for MPI functions
 */
namespace MyMPI {  // NOLINT

/*!
 * @brief Returns the rank of the process.
 * @return int
 */
int rank();

/*!
 * @brief Returns the size of the communicator.
 * @return int
 */
int size();

/*!
 * @brief Returns the size of the datatype.
 * @param datatype
 * @return int
 */
int datatype_size(MPI_Datatype datatype);

/*!
 * @brief Changes the MPI error handler to generate a segmentation fault, detectable by gdb. Easy to
 * find mpi error code line.
 * @param comm
 * @param errcode
 * @param ...
 */
void debug_errhandler(MPI_Comm *comm, int *errcode, ...);  // NOLINT

}  // namespace MyMPI

/*!
 * @brief Transform a data array into a string to be print.
 * @tparam T Data type
 * @param data Data array
 * @param size Array size
 * @return std::string String with the data
 */
template <typename T>
std::string make_string(T *data, int size) {
  std::string str;
  for (int i = 0; i < size; i++) {
    str += std::to_string(data[i]);
    str += " ";
  }
  return str;
}