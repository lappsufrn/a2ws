#pragma once

class Statistics {
#ifdef STATS
  inline static double m_time      = 0.0; /** Auxilary Time */
  inline static double m_totalTime = 0.0; /** Total Time */

  inline static unsigned m_successStealCount = 0; /** Number of successful steals */
  inline static unsigned m_failStealCount    = 0; /** Number of failed steals */
  inline static unsigned m_stolenTasks       = 0; /** Number of stolen tasks */

  inline static double m_time_rf               = 0.0; /** Auxilary Time of refresh */
  inline static double m_refreshTimeSum        = 0.0; /** Sum of refresh time */
  inline static unsigned m_successrefreshCount = 0;   /** Successful Number of refresh executed */
  inline static unsigned m_failrefreshCount    = 0;   /** Number of refresh Failed */

  inline static double m_time_ws             = 0.0; /** Auxilary Time of work stealing */
  inline static double m_tokenDelaySum       = 0.0; /** Sum of token delay */
  inline static double m_workStealingTimeSum = 0.0; /** Sum of work stealing time */
  inline static unsigned m_workStealingCount = 0;   /** Number of work stealing executed */

  inline static double m_time_gt        = 0.0;     /** Auxilary Time of get task */
  inline static double m_getTaskTimeSum = 0.0;     /** Sum of get task time */
  inline static unsigned m_getTaskCount = 0;       /** Number of get task executed */
  inline static double *p_taskTimeList  = nullptr; /** List of task time */

#endif

 public:
  static void start();
  static void end();

  static void print();
  static void reset();
  static void start_getTask();
  static void end_getTask();
  static void start_workStealing();
  static void end_workStealing();
  static void success_steal(int nts);
  static void fail_steal();
  static void end_token();
  static void start_refresh();
  static void end_refresh(bool flag);

  Statistics();
  ~Statistics();
};