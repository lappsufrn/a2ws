/*!
 * @file A2WS.hpp
 * @brief Implementation of the Adaptive Asynchronous Work-Stealing scheduler.
 *
 * This file contains the definition of the A2WS class, which implements
 * a Adaptive Asynchronous Work-Stealing scheduler for task-based parallelism in a distributed MPI
 * environment.
 */
#pragma once
#include <mpi.h>
#include <omp.h>
#include <pthread.h>
#include <unistd.h>

#include <cmath>
#include <iostream>
#include <vector>

#include "../include/a2ws/Scheduler.hpp"
#include "a2ws/InfoComm.hpp"

/**
 * @brief Adaptive Asynchronous Work-Stealing class with Windows MPI method. Creates the struct of
 * Work Stealing and apply One-Sided communication (MPI)
 */
class A2WS : public SDIS::Scheduler {

  int m_nTasks_g; /** Global sum of number of tasks */
  int m_nProcess; /** Number of process */
  int m_ID;       /** Process ID */

  // A2WS
  int m_prevID;         /** Previous process id */
  int m_nextID;         /** Next process id */
  int m_taskCount;      /** Number of tasks executed */
  double m_taskTimeSum; /** Summation of time of all tasks executed */
  double time_zero;     /** Time zero */
  double m_rateSum;     /** Summation of rate */
  double m_taskSum;     /** Summation of tasks */

  // Neighborhood
  double *p_excepTimeInfo; /** Exception of processes info */
  WSNodeInfo *p_procsInfo; /** List of processes info */
  InfoComm *p_infocomm;    /** Radius Communication */

  // Windows
  MPI_Win m_procsInfoWin; /** Processes info window */

  double exception_time(int i) const {
    return p_procsInfo[i].task_time > 0.0 ? p_procsInfo[i].task_time : p_excepTimeInfo[i];
  }

  double n_task_deduction(int i) const {
    return (static_cast<double>(p_procsInfo[i].num_tasks) -
            ((MPI_Wtime() - time_zero) * (1 / exception_time(i))));
  }

  double steal_task_rate(int i) const {
    return (((1 / exception_time(i)) * m_taskSum) / m_rateSum) -
           static_cast<double>(p_procsInfo[i].num_tasks);
  }

  /**
   * @brief Compare which case results in smaller runtime, ⌊nts⌋ or ⌈nts⌉. If equal return ⌈nts⌉.
   *
   * @param nts Number of tasks to steal to be rounded
   * @param victim Process ID of the victim
   * @return Rounded number of tasks to steal
   */
  int roundI(double nts, int victim);

  /**
   * @brief Steal tasks from the victim process.
   *
   * @param victim Process ID of the victim
   * @param nts Number of tasks to steal
   * @return 1 if the steal was successful, 0 otherwise
   */
  int stealTasks(int victim, int nts);

  /**
   * @brief Find the victim process to steal tasks.
   *
   * @param nts Number of tasks to steal
   * @return Process ID of the victim
   */
  int findVictim(double &nts);

  /**
   * @brief Calculate the number of tasks to steal in a Global communicator context.
   *
   * @return Number of tasks to steal
   */
  double calculateTasksToSteal();

  /**
   * @brief Calculate the number of tasks to steal from a specific process.
   *
   * @param victim Process ID of the victim
   * @return Number of tasks to steal
   */
  double calculateLocalTasksToSteal(int victim);

  /**
   * @brief Select a random victim from the weights.
   *
   * @param victim Process ID of the victim
   * @return Number of tasks to steal
   */
  int randomVictim(const std::vector<int> &ranks, const std::vector<double> &weights) const;

  /**
   * @brief Refresh the local process information.
   */
  void refreshProcessInfo();

  /**
   * @brief Check if local information was refreshed.
   */
  bool isUpdateNeeded() const;

  /**
   * @brief Perform the work-stealing.
   *
   * @param task Task to be executed
   * @return True if the work-stealing was successful, false otherwise
   */
  bool workstealing(void *task);

  /**
   * ************************
   * **** PUBLIC METHODS ****
   * ************************
   */

 public:
  A2WS(const A2WS &) = delete;
  A2WS(A2WS &&) = delete;
  A2WS &operator=(const A2WS &) = delete;
  A2WS &operator=(A2WS &&) = delete;

  /**
   * @brief Construct a new A2WS object
   *
   * @param n_task Number of tasks
   */
  explicit A2WS(int n_task);

  /**
   * @brief Construct a new A2WS object
   *
   * @param n_task Number of tasks
   * @param n_process Number of processes
   * @param id Process ID
   */
  A2WS(int n_task, int n_process, int id);

  /**
   * @brief Construct a new A2WS object
   *
   * @param n_task Number of tasks
   * @param n_process Number of processes
   * @param id Process ID
   * @param radius_size Radius communicator size
   */
  A2WS(int n_task, int n_process, int id, int radius_size);

  /**
   * @brief Get a task from the scheduler.
   *
   * @param task Current task ID to be executed. The list is empty if task equal to -1.
   */
  void getTask(void *task) override;

  /**
   * @brief Checking some scheduler informations when necessary.
   *
   * This method needs to be executed multiple times during the execution of tasks. Therefore, it is
   * more appropriate to place it inside the task execution.
   */
  bool refresh() override;

#ifdef VERBOSE
  /**
   * @brief Method to print verbose informations about the scheduler. Only available if VERBOSE
   * compiler flag is set.
   */
  void print() const override;
#endif

  /**
   * @brief Scheduler destructor
   */
  ~A2WS() override;

  std::string NodesInfoToString();

  bool finalize() {
    for (int i = p_infocomm->first(); i != p_infocomm->last(); i = p_infocomm->next(i)) {
      if (n_task_deduction(i) > 0.0) {
        return false;
      }
    }
    return true;
  };
};