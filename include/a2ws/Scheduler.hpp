/*!
 * @file Scheduler.hpp
 * @brief Implementation of the scheduler.
 *
 * This file contains the definition of the Scheduler abstract class
 */

#ifndef SCHEDULER_H
#define SCHEDULER_H

#include "TaskList.hpp"

namespace SDIS {

/*!
 * @class Scheduler
 * @brief Abstract base class for schedulers.
 *
 * The Scheduler class provides an interface for implementing different scheduling algorithms.
 * It defines pure virtual functions that need to be implemented by derived classes.
 * The class also provides some optional functions for checking scheduler information and printing
 * verbose information.
 */
class Scheduler {
 protected:
  bool m_end                      = false;   /*! Flag to indicate the end of the scheduler. */
  TaskList *p_queue               = nullptr; /*! Task list of each process. */
  int m_mpiExternalInitialization = true;    /*! Flag to indicate if MPI was initiated. */

 public:
  Scheduler &operator=(const Scheduler &other) = delete;
  Scheduler &operator=(Scheduler &&other)      = delete;
  Scheduler(const Scheduler &other)            = delete;
  Scheduler(Scheduler &&other)                 = delete;

  /*!
   * @brief Get a task from the scheduler.
   * @pure
   * @param task Current task to be executed.
   */
  virtual void getTask(void *task) = 0;

  void getTask(int &task) { getTask(&task); };

  /*!
   * @brief Get a task from the scheduler.
   * @tparam T Task type
   * @param task T Task
   */
  template <typename T>
  void getTask(T &task) { getTask(&task); };

  /*!
   * @brief Checking some sheduler informations when necessery.
   *
   * This method needs to be executed multiple times during the execution of tasks. Therefore, it is
   * more appropriate to place it inside the task execution.
   *
   * Example:
   * @code
   * sched->getTask(task);
   * while (!sched->end()) {
   *    printf("Task %i\n", task);
   *    for (int i = 0; i < N; i++) {
   *        sched->refresh();
   *    }
   *    sched->getTask(task);
   * }
   * @endcode
   */
  virtual bool refresh() { return false; };

  /*!
   * @brief Check if the scheduler has finished.
   *
   * @return True if the scheduler has finished, false otherwise.
   */
  bool end() const { return m_end; };

#ifdef VERBOSE
  /*!
   * @brief Method to print verbose informations about the scheduler.
   */
  virtual inline void print() const {};
#endif

  /*!
   * @brief Scheduler constructor, by default it initializes MPI if not initialized.
   */
  Scheduler() {
    MPI_Initialized(&m_mpiExternalInitialization);
    if (!m_mpiExternalInitialization) {
      int provided;
      MPI_Init_thread(nullptr, nullptr, MPI_THREAD_MULTIPLE, &provided);
    }
  };

  /*!
   * @brief Scheduler destructor
   */
  virtual ~Scheduler() {
    if (!m_mpiExternalInitialization) {
      MPI_Finalize();
    }
  };
};

}  // namespace SDIS

#endif  // SCHEDULER_H