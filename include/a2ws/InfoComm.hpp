/**
 * @file InfoComm.hpp
 * @author João Batista Fernandes (jotabe.150@gmail.com
 * @brief Implements the communication of information between processes needed for the A2 work
 * stealing algorithm.
 * @version 0.1
 * @date 2024-06-08
 *
 * @copyright Copyright (c) 2024
 */
#pragma once

#include <mpi.h>

#include <cstdint>  // std::uint8_t
#include <string>   // std::string

enum FlagMode : uint8_t { NO_UPDATE = 0x00, UPDATE = 0x01, FINISH = 0x02 };

/**
 * @brief Structure to store the information of a process.
 */
struct WSNodeInfo {
  double task_time; /** Average task runtime */
  int num_tasks;    /** Number of tasks remaining */
  uint8_t flag;     /** Flag to check if the two previous info was updated */

  static int size() { return 3; }
  static int length(int i) {
    const int len[]{1, 1, 1};
    return len[i];
  }
  static MPI_Datatype type(int i) {
    const MPI_Datatype types[] = {MPI_DOUBLE, MPI_INT, MPI_UINT8_T};
    return types[i];
  }
  static MPI_Aint disp(int i) {
    const MPI_Aint disp[] = {offsetof(WSNodeInfo, task_time), offsetof(WSNodeInfo, num_tasks),
                             offsetof(WSNodeInfo, flag)};
    return disp[i];
  }
};

/*!
 * @brief Interface to manage the communication of information between processes for the A2
 * work-stealing.
 */
class InfoComm {

 public:
  InfoComm(const InfoComm &)            = delete;
  InfoComm(InfoComm &&)                 = delete;
  InfoComm &operator=(const InfoComm &) = delete;
  InfoComm &operator=(InfoComm &&)      = delete;

  /**
   * @brief Enum to define the communication mode.
   *  GLOBAL_COMM: Include all processes
   *  LOCAL_COMM: Include only neighbors
   */
  enum CommMode : std::uint8_t {
    GLOBAL_COMM = 0x01, /** Include all processes */
    LOCAL_COMM  = 0x02  /** Include only neighbors */
  };

  /**
   * @brief Construct a new Info Comm object
   */
  InfoComm() = default;

  /**
   * @brief Destroy the Info Comm object
   */
  virtual ~InfoComm() = default;

  /**
   * @brief Get the first process ID
   * @return int
   */
  virtual int first() const = 0;

  /**
   * @brief Get the last process ID
   * @return int
   */
  virtual int last() const = 0;

  /**
   * @brief Get the process ID
   * @return int
   */
  virtual int size() const = 0;

  /**
   * @brief Get the next process ID in the ring.
   * @param id Process ID
   * @return int Next process ID
   */
  virtual int next(int id) const = 0;

  /**
   * @brief Get the communication mode
   * @return CommMode
   */
  virtual CommMode mode() const = 0;

  /**
   * @brief Send information to the neighborhood processes.
   * @param info Information list to be sent
   * @param win info mpi window
   * @return true if the information was sent, false otherwise
   */
  virtual bool updateInfo(WSNodeInfo *info, const MPI_Win &win) = 0;
};