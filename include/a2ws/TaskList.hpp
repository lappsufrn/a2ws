/**
 * @file TaskList.hpp
 * @author João Batista (jotabe.150@gmail.com)
 * @brief Contain the class TaskList inside the namespace SDIS
 * @version 0.1
 * @date 2024-05-10
 *
 * @copyright Copyright (c) 2024
 *
 */
#pragma once

#include <mpi.h>

#include <cstddef>
#include <cstdint>
#include <iostream>

#define exist(_VICTIM_) (_VICTIM_ != -1)

namespace SDIS {

/**
 * @brief Struct of Work Stealing TaskList Limits. Contains the head and tail of the tasks.
 */
struct Limits {
  int head; /** Head of the queue */
  int tail; /** Tail of the queue */
};

/**
 * @brief Class to the task queue. Implemented with head and tail limits and contains the MPI
 * windowns.
 */
class TaskList {
 public:
  enum ListMode : std::uint8_t {
    DEFAULT = 0x01, /** Normal list mode */
    MPIWIN  = 0x02  /** MPI win list mode */
  };

 private:
#ifdef SDIS_TASK_HEADER
  struct TaskHeader {
    unsigned int id; /** Task id */
  };
#endif

#ifdef SDIS_TASK_HEADER
  inline static int m_headerSize = sizeof(TaskHeader); /** Size of the head */
#else
  inline static int m_headerSize = 0; /** Size of the head */
#endif

  const int m_taskSize;  /** Size of the task */
  const int m_maxSize;   /** Maximum size of the queue */
  const ListMode m_mode; /** Mode of the list, normal or MPI win */
  Limits *p_limits;      /** Limits of the queue (head and tail) */
  std::byte *p_taskList; /** List of tasks */

  MPI_Win m_listWin   = MPI_WIN_NULL; /** MPI Window for the list */
  MPI_Win m_headWin   = MPI_WIN_NULL; /** MPI Window for the head */
  MPI_Win m_tailWin   = MPI_WIN_NULL; /** MPI Window for the tail */
  MPI_Win m_limitsWin = MPI_WIN_NULL; /** MPI Window for the limits */

  MPI_Datatype m_listType; /** MPI Datatype for the task list */

#ifdef DEBUG
  void *p_listTasks_end = nullptr; /** End of the list */
#endif

 public:
  TaskList()                                 = delete;
  TaskList(const TaskList &other)            = delete;
  TaskList(TaskList &&other)                 = delete;
  TaskList &operator=(TaskList &&other)      = delete;
  TaskList &operator=(const TaskList &other) = delete;

  /**
   * @brief Construct a new Task List object
   * @param ntask Maximum number of tasks in the queue
   * @param datatype List MPI Datatype
   * @throw invalid_argument(SDIS_NTASK_ERROR_MESSAGE) Number of tasks (n_task) should be greater
   * than zero!
   */
  TaskList(int ntask, MPI_Datatype datatype);

  /**
   * @brief Construct a new Task List object
   *
   * @param ntask Maximum number of tasks in the queue
   * @param datatype List MPI Datatype
   * @param mode List mode
   *    - DEFAULT: Normal list mode
   *    - MPIWIN: MPI win list mode
   *
   * @throw invalid_argument(SDIS_NTASK_ERROR_MESSAGE) Number of tasks (n_task) should be greater
   * than zero!
   * @throw invalid_argument(SDIS_TASK_SIZE_ERROR_MESSAGE) Task size should be greater than zero!
   * @throw invalid_argument(SDIS_QUEUE_MODE_ERROR_MESSAGE) Task list mode type does not exist
   */
  TaskList(int ntask, MPI_Datatype datatype, ListMode mode);

  /**
   * @brief Construct a new TaskList object. The list will be created in the using default c++
   * new operator (DEFAULT mode).
   * @throw invalid_argument(SDIS_NTASK_ERROR_MESSAGE) Number of tasks (n_task) should be
   * greater than zero!
   */
  explicit TaskList(int ntask);

  /**
   * @brief Construct a new TaskList object. The list will be created in the using default c++ new
   * operator (DEFAULT mode).
   * @param ntask Maximum number of tasks in the queue
   * @param task_size Size of the task
   * @throw invalid_argument(SDIS_NTASK_ERROR_MESSAGE) Number of tasks (n_task) should be greater
   * than zero!
   * @throw invalid_argument(SDIS_TASK_SIZE_ERROR_MESSAGE) Task size should be greater than zero!
   */
  TaskList(int ntask, int task_size);

  /**
   * @brief Destroy the TaskList object
   */
  ~TaskList();

  /**
   * @brief TaskList task list access operator
   * @attention MPI: The function will read the list in the MPI window.
   * @param index Task list position
   * @throw out_of_range(SDIS_QUEUE_INDEX_ERROR_MESSAGE)
   * @return std::byte Task
   */
  std::byte operator[](int index) const;

  /**
   * @brief Move the tail of the queue
   * @attention MPI: The function will read and write the tail value in the MPI window.
   * @param n Number of positions to move
   * @throw out_of_range(SDIS_QUEUE_INDEX_ERROR_MESSAGE)
   */
  void move_tail(int n);

  /**
   * @brief TaskList task list output operator
   * @param os Output stream
   * @return std::ostream& Output stream
   */
  std::ostream &operator<<(std::ostream &os);

  template <typename T>
  std::string print() const {
    std::string str;
    for (int i = 0; i < get_tail(); ++i) {
      if (i == get_head()) {
        str += "[";
        str += std::to_string(static_cast<T>(p_taskList[i * m_taskSize]));
        str += "] ";
      } else {
        str += std::to_string(static_cast<T>(p_taskList[i * m_taskSize]));
        str += " ";
      }
    }
    return str;
  }

  /**
   * @brief Get the tail object
   * @return tail id
   */
  int get_tail() const { return p_limits->tail; }

  /**
   * @brief Get the head object
   * @return head id
   */
  int get_head() const { return p_limits->head; }

  /**
   * @brief Get the mode object
   * @return int Mode of the list
   */
  int get_mode() const { return m_mode; }

  /**
   * @brief Get the max size object
   * @return int Maximum size of the queue
   */
  int get_max_size() const { return m_maxSize; }

  /**
   * @brief Get the listType object
   * @return MPI_Datatype MPI Datatype for the list
   */
  MPI_Datatype get_list_type() const { return m_listType; }

  /**
   * @brief Get the type size object
   * @return int Size of the task
   */
  int get_type_size() const { return m_taskSize; }

  /**
   * @brief Get the listWin object
   * @throw domain_error(SDIS_QUEUE_DEFAULT_MODE_ERROR_MESSAGE) Method not available in the DEFAULT
   * mode.
   * @return MPI_Win MPI Window for the list
   */
  MPI_Win get_listWin() const { return m_listWin; }

  /**
   * @brief Get the headWin object
   * @throw domain_error(SDIS_QUEUE_DEFAULT_MODE_ERROR_MESSAGE) Method not available in the DEFAULT
   * mode.
   * @return MPI_Win MPI Window for the head
   */
  MPI_Win get_head_win() const { return m_headWin; }

  /**
   * @brief Get the tailWin object
   * @throw domain_error(SDIS_QUEUE_DEFAULT_MODE_ERROR_MESSAGE) Method not available in the DEFAULT
   * mode.
   * @return MPI_Win MPI Window for the tail
   */
  MPI_Win get_tail_win() const { return m_tailWin; }

  /**
   * @brief Get the limitsWin object
   * @return MPI_Win MPI Window for the limits
   */
  MPI_Win get_limits_win() const { return m_limitsWin; }

  /**
   * @brief Get the size of the queue
   * @attention MPI: The function will read the head and tail values in the MPI window.
   * @return int Size of the queue
   */
  int size() const;

  /**
   * @brief Get the front object and remove it from the queue
   * @attention MPI: The function will read and write the head value and read the list in the MPI
   * window.
   * @param data Task
   */
  void front_pop(void *&data);

  /**
   * @brief Check if the queue is empty.
   * @attention MPI: The function will read the head and tail values in the MPI window.
   * @return true If the queue is empty
   * @return false If the queue is not empty
   */
  bool empty() const;

  /**
   * @brief Push a task to the queue.
   * @attention MPI: The function will read and write the tail and the list in the MPI window.
   * @param data Task
   */
  void back_push(const void *data);

  /**
   * @brief Copy a list of tasks to the queue
   * @param data List of tasks
   * @param size Number of tasks
   */
  void back_copy(const void *data, int size);

  /**
   * @brief Clear the queue
   */
  void clear();

  /**
   * @brief Adjust the queue limits if the tail is lesser than the head (not usual case)
   */
  void adjust();
};

};  // namespace SDIS