# A2WS

## Running Example

Installing the A2WS library

```sh
$ export PROJECT_DIR=$(pwd)
$ mkdir build && cd build
$ cmake ../ -DCMAKE_INSTALL_PREFIX=${PROJECT_DIR}/a2ws/ 
$ make -j install
```

Installing the example

```sh
$ export PROJECT_DIR=$(pwd)
$ cd example
$ mkdir -p build && cd build
$ cmake ../ -DCMAKE_INSTALL_PREFIX=${PROJECT_DIR} -DCMAKE_PREFIX_PATH=${PROJECT_DIR}/a2ws/
$ make -j install
```

Running the example

```sh
$ export LD_LIBRARY_PATH=$PWD/a2ws/lib/
$ mpirun -np <number-of-process> ./bin/modeling
```

Exempected output

```txt
--- Running shot 75
--- Running shot 0
--- Running shot 25
[...]
--- Running shot 49
--- Running shot 24
--- Running shot 99
```

Are 100 shots in total.
